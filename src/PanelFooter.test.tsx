import {waitFor, render, screen, fireEvent} from '@testing-library/react';
import PanelFooter from './PanelFooter';
import PanelEnded from './PanelEnded';

it('should properly rendered without props', async () => {
  const setCurrentPage = jest.fn();
  const updatePanel = jest.fn();
  render(
    <PanelFooter setCurrentPage={setCurrentPage} updatePanel={updatePanel} />
  );
  await waitFor(() => {
    expect(screen.getByTestId('page-desc')).toBeInTheDocument();
    expect(screen.getByTestId('page-desc')).toHaveTextContent(
      '共 0 筆，目前位於 0 頁中的'
    );
    expect(screen.getByTestId('select')).not.toHaveValue();
    expect(screen.getByTestId('select').childNodes.length).toBe(0);
  });
});

it('should properly rendered without any item', async () => {
  const setCurrentPage = jest.fn();
  const updatePanel = jest.fn();
  render(
    <PanelFooter
      itemCount={0}
      itemsPerPage={10}
      currentPage={1}
      setCurrentPage={setCurrentPage}
      updatePanel={updatePanel}
    />
  );
  await waitFor(() => {
    expect(screen.getByTestId('page-desc')).toBeInTheDocument();
    expect(screen.getByTestId('page-desc')).toHaveTextContent(
      '共 0 筆，目前位於 0 頁中的'
    );
    expect(screen.getByTestId('select')).not.toHaveValue();
    expect(screen.getByTestId('select').childNodes.length).toBe(0);
  });
});

it('should properly rendered when item = 5', async () => {
  const setCurrentPage = jest.fn();
  const updatePanel = jest.fn();
  render(
    <PanelFooter
      itemCount={5}
      itemsPerPage={10}
      currentPage={1}
      setCurrentPage={setCurrentPage}
      updatePanel={updatePanel}
    />
  );
  await waitFor(() => {
    expect(screen.getByTestId('page-desc')).toBeInTheDocument();
    expect(screen.getByTestId('page-desc')).toHaveTextContent(
      '共 5 筆，目前位於 1 頁中的'
    );
    expect(screen.getByTestId('select')).toHaveValue('1');
    expect(screen.getByTestId('select').childNodes.length).toBe(1);
    expect(screen.getByText('第 1 頁')).toBeTruthy();
  });
});

it('should properly rendered when item = 10', async () => {
  const setCurrentPage = jest.fn();
  const updatePanel = jest.fn();
  render(
    <PanelFooter
      itemCount={10}
      itemsPerPage={10}
      currentPage={1}
      setCurrentPage={setCurrentPage}
      updatePanel={updatePanel}
    />
  );
  await waitFor(() => {
    expect(screen.getByTestId('page-desc')).toBeInTheDocument();
    expect(screen.getByTestId('page-desc')).toHaveTextContent(
      '共 10 筆，目前位於 1 頁中的'
    );
    expect(screen.getByTestId('select')).toHaveValue('1');
    expect(screen.getByTestId('select').childNodes.length).toBe(1);
    expect(screen.getByText('第 1 頁')).toBeTruthy();
  });
});

it('should properly rendered when item = 15', async () => {
  const setCurrentPage = jest.fn();
  const updatePanel = jest.fn();
  render(
    <PanelFooter
      itemCount={15}
      itemsPerPage={10}
      currentPage={1}
      setCurrentPage={setCurrentPage}
      updatePanel={updatePanel}
    />
  );
  await waitFor(() => {
    expect(screen.getByTestId('page-desc')).toBeInTheDocument();
    expect(screen.getByTestId('page-desc')).toHaveTextContent(
      '共 15 筆，目前位於 2 頁中的'
    );
    expect(screen.getByTestId('select')).toHaveValue('1');
    expect(screen.getByTestId('select').childNodes.length).toBe(2);
    expect(screen.getByText('第 1 頁')).toBeTruthy();
    expect(screen.getByText('第 2 頁')).toBeTruthy();
  });
});

it('should update currentPage when pager menu was selected', async () => {
  render(
    <PanelEnded
      idHash={'989db2448f309bfdd99b513f37c84b8f5794d2b5'}
      api="/api/wiki_crawler/list"
      type="WikiCrawler"
    ></PanelEnded>
  );
  await waitFor(
    async () => {
      fireEvent.change(screen.getByTestId('select'), {target: {value: '2'}});
      await waitFor(() => {
        const options = screen.getAllByTestId('select-option');
        expect(options[0]['selected']).toBeFalsy();
        expect(options[1]['selected']).toBeTruthy();
        expect(screen.getByTestId('select')).toHaveValue('2');
      });
    },
    {timeout: 5000}
  );
});
