import React from 'react';
import {CardHeader} from 'reactstrap';

type PanelHeaderProps = {
  title: string;
  errorMsg: string;
};

const defaultProps = {
  title: 'PanelHeaderTitle',
  errorMsg: '',
};

const PanelHeader = ({title, errorMsg}: PanelHeaderProps) => {
  return (
    <CardHeader className="d-flex justify-content-between bg-primary">
      <span className="text-light" data-testid="header-title">
        {title}
      </span>
      {!!errorMsg && (
        <span className="text-danger-light" data-testid="header-error">
          發生錯誤！ {errorMsg}
        </span>
      )}
    </CardHeader>
  );
};

PanelHeader.defaultProps = defaultProps;

export default PanelHeader;
