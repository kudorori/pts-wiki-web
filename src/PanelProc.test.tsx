import {render, screen, waitFor} from '@testing-library/react';
import PanelProc from './PanelProc';

it('should properly rendered when type="WikiCrawler"', async () => {
  const updateIdHash = jest.fn();
  const needUpdatePanel = Date.now();
  render(
    <PanelProc
      pollingApi="/api/wiki_crawler/process_list"
      pollingIntervalSec={3}
      abortApi="/api/wiki_crawler/list/"
      type="WikiCrawler"
      updateIdHash={updateIdHash}
      needUpdatePanel={needUpdatePanel}
    />
  );
  await waitFor(
    () => {
      expect(screen.getByText('https://zh.wikipedia.org')).toBeInTheDocument();
    },
    {timeout: 5000}
  );
});

it('should properly rendered when type="Teaching"', async () => {
  const updateIdHash = jest.fn();
  const needUpdatePanel = Date.now();
  render(
    <PanelProc
      pollingApi="/api/teaching/process_list"
      pollingIntervalSec={3}
      abortApi="/api/teaching/list/"
      type="Teaching"
      updateIdHash={updateIdHash}
      needUpdatePanel={needUpdatePanel}
    />
  );
  await waitFor(
    () => {
      expect(screen.getByText(/FaceBox 網址/)).toBeInTheDocument();
    },
    {timeout: 5000}
  );
});

it('should properly rendered without any task', async () => {
  const updateIdHash = jest.fn();
  const needUpdatePanel = Date.now();
  render(
    <PanelProc
      pollingApi="/api/empty"
      pollingIntervalSec={3}
      abortApi="/api/empty"
      type="WikiCrawler"
      updateIdHash={updateIdHash}
      needUpdatePanel={needUpdatePanel}
    />
  );
  await waitFor(
    () => {
      expect(screen.getByText('目前沒有工作')).toBeInTheDocument();
    },
    {timeout: 5000}
  );
});

it('should properly rendered when get error response from server', async () => {
  const updateIdHash = jest.fn();
  const needUpdatePanel = Date.now();
  jest.useFakeTimers();
  render(
    <PanelProc
      pollingApi="/wrong/api"
      pollingIntervalSec={3}
      abortApi="/wrong/api"
      type="WikiCrawler"
      updateIdHash={updateIdHash}
      needUpdatePanel={needUpdatePanel}
    />
  );
  await waitFor(
    () => {
      expect(screen.getByText('發生錯誤')).toBeInTheDocument();
      jest.advanceTimersByTime(10000);
      expect(setTimeout).toHaveBeenCalledTimes(4);
    },
    {timeout: 5000}
  );
});
