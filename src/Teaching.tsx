import React, {useState} from 'react';
import {buildIdHash} from './utils';
import TeachingCreate from './TeachingCreate';
import PanelProc from './PanelProc';
import PanelEnded from './PanelEnded';

const Teaching = () => {
  const [idHash, updateIdHash] = useState(buildIdHash([]));
  const [needUpdatePanel, setNeedUpdatePanel] = useState(Date.now());
  return (
    <>
      <TeachingCreate
        createApi="/api/teaching"
        setNeedUpdatePanel={setNeedUpdatePanel}
      />
      <PanelProc
        pollingApi="/api/teaching/process_list"
        pollingIntervalSec={3}
        abortApi="/api/teaching/list"
        type="Teaching"
        updateIdHash={updateIdHash}
        needUpdatePanel={needUpdatePanel}
      />
      <PanelEnded idHash={idHash} api="/api/teaching/list" type="Teaching" />
    </>
  );
};

export default Teaching;
