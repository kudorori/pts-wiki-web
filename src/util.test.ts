import {
  getErrorMsg,
  getStatusDisplay,
  buildIdHash,
  checkValidUrl,
  checkValidDepth,
  checkValidFile,
} from './utils';

it('getErrorMsg() should return proper error message when get error code 400', () => {
  const fakeResponse = {
    response: {
      status: 400,
      data: {
        errCode: 0,
        errMessage: 'errmsg',
      },
    },
  };
  expect(getErrorMsg(fakeResponse)).toBe('錯誤碼：0，errmsg');
});

it('getErrorMsg() should return proper error message when get error code 500', () => {
  const fakeResponse = {
    response: {
      status: 500,
      data: {
        errCode: 0,
        errMessage: 'errmsg',
      },
    },
  };
  expect(getErrorMsg(fakeResponse)).toBe('未知的異常，請聯絡管理者。');
});

it('getStatusDisplay() should return proper object when status="FINISHED"', () => {
  expect(getStatusDisplay('FINISHED').text).toBe('已完成');
});

it('getStatusDisplay() should return proper object when status="ABORTED"', () => {
  expect(getStatusDisplay('ABORTED').text).toBe('已中斷');
});

it('getStatusDisplay() should return proper object when status=""', () => {
  expect(getStatusDisplay('').text).toBe('狀態未知');
});

it('buildIdHash() should return proper hash when input array=[]', () => {
  expect(buildIdHash([])).toBe('989db2448f309bfdd99b513f37c84b8f5794d2b5');
});

it('buildIdHash() should return proper hash when input array=[{id: 1}]', () => {
  expect(buildIdHash([{id: 1}])).toBe(
    'ad26d30eeb389bcff610f00b8c2362d9061f4762'
  );
});

it('checkValidUrl() should return true when url="https://zh.wikipedia.org/wiki/中華民國"', () => {
  expect(
    checkValidUrl(
      'https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8B'
    )
  ).toBe(true);
});

it('checkValidUrl() should return false when url="https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8"', () => {
  expect(
    checkValidUrl(
      'https://zh.wikipedia.org/wiki/%E4%B8%AD%E8%8F%AF%E6%B0%91%E5%9C%8'
    )
  ).toBe(false);
});

it('checkValidDepth() should return true when input 2', () => {
  expect(checkValidDepth(2)).toBe(true);
});

it('checkValidDepth() should return false when input 1.1', () => {
  expect(checkValidDepth(1.1)).toBe(false);
});

it('checkValidDepth() should return false when input 0', () => {
  expect(checkValidDepth(0)).toBe(false);
});

it('checkValidDepth() should return false when input -1', () => {
  expect(checkValidDepth(-1)).toBe(false);
});

it('checkValidFile() should return true when input test.json', () => {
  expect(checkValidFile('test.json')).toBe(true);
});

it('checkValidFile() should return true when input test.JSON', () => {
  expect(checkValidFile('test.JSON')).toBe(true);
});

it('checkValidFile() should return false when input test.txt', () => {
  expect(checkValidFile('test.txt')).toBe(false);
});

it('checkValidFile() should return false when input test.pdf', () => {
  expect(checkValidFile('test.pdf')).toBe(false);
});
