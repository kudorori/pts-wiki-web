import React from 'react';
import {ListGroupItem, Button, Progress} from 'reactstrap';
import axios from 'axios';
import {getErrorMsg, taskItem, apiType} from './utils';

type PanelItemProcProps = {
  task: taskItem;
  type: apiType;
  abortApi: String;
  updatePanel?: Function;
};

const defaultProps = {
  task: {
    index: 0,
    id: 0,
    createdAt: '1900/01/01 00:00:00',
    doneAt: '2020/12/31 23:59:59',
    searchUrl: 'https://zh.wikipedia.org',
    depthNum: 1,
    fileName: 'upload.json',
    faceBoxUrl: 'https://facebox.com/01',
    tagBoxUrl: 'https://tagbox.com/01',
    failedMessage: '',
    progressCount: 0,
    progressTotal: 100,
    status: 'PROCESSING',
  },
  type: 'WikiCrawler',
  abortApi: '',
};

const PanelItemProc = ({
  task,
  type,
  abortApi,
  updatePanel,
}: PanelItemProcProps) => {
  const progressPercent = (task.progressCount / task.progressTotal) * 100;
  const progressPercentDisplay =
    Math.floor(progressPercent) === 0
      ? '準備中'
      : `${Math.floor(progressPercent).toString()}%`;

  const deleteTask = (clickEvent: React.SyntheticEvent): void => {
    const taskId = clickEvent.currentTarget.getAttribute('task-id');
    axios
      .delete(`${abortApi}/${taskId}`)
      .then(() => {
        updatePanel();
      })
      .catch(error => {
        alert(`發生錯誤！ ${getErrorMsg(error)}`);
      });
  };

  const infoColWikiCrawler = (task: taskItem): JSX.Element | JSX.Element[] => {
    const searchUrlDisplay = decodeURI(task.searchUrl);
    return (
      <a
        href={task.searchUrl}
        target="_blank"
        rel="noreferrer"
        data-testid="search-url"
      >
        {searchUrlDisplay}
      </a>
    );
  };

  const infoColTeaching = (task: taskItem): JSX.Element | JSX.Element[] => {
    return (
      <>
        <div className="mr-4 d-flex" style={{minWidth: '160px'}}>
          <span className="d-flex flex-column justify-content-center align-items-end small text-secondary">
            檔案：
          </span>
          <span
            className="d-flex flex-column justify-content-center align-items-start small"
            data-testid="file-name"
          >
            {task.fileName}
          </span>
        </div>
        <div className="d-flex">
          <span
            className="d-flex flex-column justify-content-center align-items-end small"
            style={{minWidth: '100px'}}
          >
            <span className="my-1 text-secondary">FaceBox 網址：</span>
            <span className="my-1 text-secondary">TagBox 網址：</span>
          </span>
          <span className="d-flex flex-column justify-content-center align-items-start small">
            <span className="my-1">{task.faceBoxUrl}</span>
            <span className="my-1">{task.tagBoxUrl}</span>
          </span>
        </div>
      </>
    );
  };

  const infoCol = (type: apiType): Function => {
    switch (type) {
      case 'WikiCrawler':
        return infoColWikiCrawler;
      case 'Teaching':
        return infoColTeaching;
    }
  };

  return (
    <ListGroupItem>
      <div className="row fadeInLeft">
        <div
          className="col-1 d-flex justify-content-center align-items-center font-weight-bold"
          data-testid="index"
        >
          {task.index}
        </div>
        <div className="col-7 d-flex justify-content-left align-items-center text-break">
          {infoCol(type)(task)}
        </div>
        <div className="col-3 d-flex flex-column justify-content-center align-items-left pl-0">
          <Progress animated value={progressPercent} />
          <div className="text-right small mt-1">
            已處理 {task.progressTotal} 筆中的 {task.progressCount} 筆 -{' '}
            {progressPercentDisplay}
          </div>
        </div>
        <div className="col-1 p-0 d-flex justify-content-center align-items-center">
          <Button
            task-id={task.id}
            color="danger"
            onClick={deleteTask}
            data-testid="btn-abort"
          >
            <i className="fas fa-times mr-2"></i>
            中止
          </Button>
        </div>
      </div>
    </ListGroupItem>
  );
};

PanelItemProc.defaultProps = defaultProps;

export default PanelItemProc;
