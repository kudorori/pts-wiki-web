import {render, screen, waitFor} from '@testing-library/react';
import PanelEnded from './PanelEnded';

it('should properly rendered without props', async () => {
  render(<PanelEnded api="/api/wiki_crawler/list" type="WikiCrawler" />);
  expect(screen.getByText(/已完成工作/)).toBeInTheDocument();
});

it('should properly rendered when type="WikiCrawler"', async () => {
  render(<PanelEnded api="/api/wiki_crawler/list" type="WikiCrawler" />);
  await waitFor(
    () => {
      expect(screen.getAllByTestId('search-url')[0]).toBeInTheDocument();
      expect(screen.getAllByTestId('search-url')[0]).toHaveTextContent(
        'https://zh.wikipedia.org'
      );
      expect(screen.getAllByTestId('index')[0]).toHaveTextContent('1');
      expect(screen.getAllByTestId('download-link')[0]).toHaveAttribute(
        'href',
        '/api/wiki_crawler/list/0/download'
      );
    },
    {timeout: 5000}
  );
});

it('should properly rendered when type="Teaching"', async () => {
  render(<PanelEnded api="/api/teaching/list" type="Teaching" />);
  await waitFor(
    () => {
      expect(screen.getAllByTestId('file-name')[0]).toBeInTheDocument();
      expect(screen.getAllByTestId('file-name')[0]).toHaveTextContent(
        'file00.json'
      );
    },
    {timeout: 5000}
  );
});

it('should properly rendered when get error code 500 from server', async () => {
  render(<PanelEnded api="wrong/url" type="WikiCrawler" />);
  await waitFor(
    () => {
      expect(screen.getByText('發生錯誤')).toBeInTheDocument();
    },
    {timeout: 5000}
  );
});
