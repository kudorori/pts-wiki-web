import {waitFor, render, screen, fireEvent} from '@testing-library/react';
import PanelItemProc from './PanelItemProc';

it('should properly rendered without props', async () => {
  render(<PanelItemProc />);
  await waitFor(() => {
    expect(screen.getByTestId('search-url')).toBeInTheDocument();
    expect(screen.getByTestId('search-url')).toHaveTextContent(
      'https://zh.wikipedia.org'
    );
    expect(screen.getByTestId('index')).toHaveTextContent('0');
  });
});

it('should properly rendered when type="WikiCrawler"', async () => {
  const updatePanel = jest.fn();
  const task = {
    index: 1,
    id: 0,
    createdAt: '1900/01/01 00:00:00',
    doneAt: '2020/12/31 23:59:59',
    searchUrl: 'https://www.google.com',
    depthNum: 1,
    fileName: 'upload.json',
    faceBoxUrl: 'https://facebox.com/01',
    tagBoxUrl: 'https://tagbox.com/01',
    failedMessage: '',
    progressCount: 0,
    progressTotal: 100,
    status: 'PROCESSING',
  };
  render(
    <PanelItemProc task={task} type="WikiCrawler" updatePanel={updatePanel} />
  );
  await waitFor(() => {
    expect(screen.getByTestId('search-url')).toBeInTheDocument();
    expect(screen.getByTestId('search-url')).toHaveTextContent(
      'https://www.google.com'
    );
    expect(screen.getByTestId('index')).toHaveTextContent('1');
    expect(updatePanel).not.toBeCalled();
  });
});

it('should properly rendered when type="Teaching"', async () => {
  const updatePanel = jest.fn();
  const task = {
    index: 1,
    id: 0,
    createdAt: '1900/01/01 00:00:00',
    doneAt: '2020/12/31 23:59:59',
    searchUrl: 'https://www.google.com',
    depthNum: 1,
    fileName: 'upload.json',
    faceBoxUrl: 'https://facebox.com/01',
    tagBoxUrl: 'https://tagbox.com/01',
    failedMessage: '',
    progressCount: 0,
    progressTotal: 100,
    status: 'PROCESSING',
  };
  render(
    <PanelItemProc task={task} type="Teaching" updatePanel={updatePanel} />
  );
  await waitFor(() => {
    expect(screen.getByTestId('file-name')).toBeInTheDocument();
    expect(screen.getByTestId('file-name')).toHaveTextContent('upload.json');
  });
  expect(updatePanel).not.toBeCalled();
});

it('should abort task when abort button was clicked', async () => {
  const updatePanel = jest.fn();
  const task = {
    index: 1,
    id: 0,
    createdAt: '1900/01/01 00:00:00',
    doneAt: '2020/12/31 23:59:59',
    searchUrl: 'https://www.google.com',
    depthNum: 1,
    fileName: 'upload.json',
    faceBoxUrl: 'https://facebox.com/01',
    tagBoxUrl: 'https://tagbox.com/01',
    failedMessage: '',
    progressCount: 0,
    progressTotal: 100,
    status: 'PROCESSING',
  };
  render(
    <PanelItemProc
      abortApi="/api/wiki_crawler/list"
      task={task}
      type="WikiCrawler"
      updatePanel={updatePanel}
    />
  );
  fireEvent.click(screen.getByTestId('btn-abort'));
  await waitFor(() => {
    expect(screen.getByTestId('search-url')).toBeInTheDocument();
    expect(screen.getByTestId('search-url')).toHaveTextContent(
      'https://www.google.com'
    );
    expect(screen.getByTestId('index')).toHaveTextContent('1');
    expect(updatePanel).toBeCalled();
  });
});

it('should trigger browser alert with proper error message when get response 400 from server', async () => {
  const updatePanel = jest.fn();
  jest.spyOn(window, 'alert').mockImplementation(() => {});
  const task = {
    index: 1,
    id: 1,
    createdAt: '1900/01/01 00:00:00',
    doneAt: '2020/12/31 23:59:59',
    searchUrl: 'https://www.google.com',
    depthNum: 1,
    fileName: 'upload.json',
    faceBoxUrl: 'https://facebox.com/01',
    tagBoxUrl: 'https://tagbox.com/01',
    failedMessage: '',
    progressCount: 0,
    progressTotal: 100,
    status: 'PROCESSING',
  };
  render(
    <PanelItemProc
      abortApi="/api/wiki_crawler/list"
      task={task}
      type="WikiCrawler"
      updatePanel={updatePanel}
    />
  );
  fireEvent.click(screen.getByTestId('btn-abort'));
  await waitFor(() => {
    expect(window.alert).toBeCalledWith('發生錯誤！ 錯誤碼：0，string');
    expect(updatePanel).not.toBeCalled();
  });
});

it('should trigger browser alert with proper error message when get response 500 from server', async () => {
  const updatePanel = jest.fn();
  jest.spyOn(window, 'alert').mockImplementation(() => {});
  const task = {
    index: 1,
    id: 1,
    createdAt: '1900/01/01 00:00:00',
    doneAt: '2020/12/31 23:59:59',
    searchUrl: 'https://www.google.com',
    depthNum: 1,
    fileName: 'upload.json',
    faceBoxUrl: 'https://facebox.com/01',
    tagBoxUrl: 'https://tagbox.com/01',
    failedMessage: '',
    progressCount: 0,
    progressTotal: 100,
    status: 'PROCESSING',
  };
  render(
    <PanelItemProc
      abortApi="/wrong/api"
      task={task}
      type="WikiCrawler"
      updatePanel={updatePanel}
    />
  );
  fireEvent.click(screen.getByTestId('btn-abort'));
  await waitFor(() => {
    expect(window.alert).toBeCalledWith(
      '發生錯誤！ 未知的異常，請聯絡管理者。'
    );
    expect(updatePanel).not.toBeCalled();
  });
});
