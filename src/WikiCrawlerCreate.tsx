import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {Card, CardBody, Button, Label, Input} from 'reactstrap';
import {checkValidUrl, checkValidDepth, getErrorMsg} from './utils';
import PanelHeader from './PanelHeader';

type taskInfo = {
  searchUrl: string;
  depthNum: string;
};

type WikiCrawlerCreateProps = {
  createApi: string;
  setNeedUpdatePanel: Function;
};

const WikiCrawlerCreate = ({
  createApi,
  setNeedUpdatePanel,
}: WikiCrawlerCreateProps) => {
  const [searchUrl, setSearchUrl] = useState('');
  const [depthNum, setDepthNum] = useState('');
  const [isValidUrl, checkUrl] = useState(false);
  const [isValidDepth, checkDepth] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const [isUploading, setIsUploading] = useState(false);

  const updateSearchUrl = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchUrl(event.target.value);
  };

  const updateDepthNum = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDepthNum(event.target.value);
  };

  const createTask = (): void => {
    setIsUploading(true);
    const taskInfo: taskInfo = {
      searchUrl,
      depthNum,
    };
    axios
      .post(createApi, taskInfo)
      .then(() => {
        setErrorMsg('');
      })
      .catch(error => {
        setErrorMsg(getErrorMsg(error));
      })
      .finally(() => {
        document.getElementById('search-url')['value'] = '';
        document.getElementById('depth-num')['value'] = '';
        setSearchUrl('');
        setDepthNum('');
        checkUrl(checkValidUrl(''));
        checkDepth(checkValidDepth(parseFloat('')));
        setNeedUpdatePanel(Date.now());
        setIsUploading(false);
      });
  };

  useEffect(() => {
    checkUrl(checkValidUrl(searchUrl));
    checkDepth(checkValidDepth(parseFloat(depthNum)));
  }, [searchUrl, depthNum]);

  const isInvalidUrl = !isValidUrl && searchUrl !== '';
  const isInvalidDepth = !isValidDepth && depthNum !== '';
  const allowSubmit =
    searchUrl !== '' && depthNum !== null && isValidUrl && isValidDepth;
  const btnColor = allowSubmit && !isUploading ? 'success' : 'secondary';
  const searchUrlDisplay = isValidUrl ? decodeURI(searchUrl) : '';
  const btnIconClass = isUploading
    ? 'fas fa-circle-notch fa-spin mr-2'
    : 'fas fa-plus mr-2';

  return (
    <Card className="pwc-status-panel-create mb-3 border-primary">
      <PanelHeader title="加入新工作" errorMsg={errorMsg} />
      <CardBody>
        <div className="d-inline-flex align-items-center w-100 pb-3">
          <Label for="search-url" className="m-0">
            欲搜尋的維基網址：
          </Label>
          <div className="d-inline mr-3 flex-grow-1">
            <Input
              id="search-url"
              className="w-100"
              placeholder="請輸入正確的網址格式"
              valid={isValidUrl}
              invalid={isInvalidUrl}
              type="url"
              onChange={updateSearchUrl}
              data-testid="search-url"
            />
            <div className="position-absolute valid-feedback">
              網址格式正確：
              <a href={searchUrl} target="_blank" rel="noreferrer">
                {searchUrlDisplay}
              </a>
            </div>
            <div className="position-absolute invalid-feedback">
              網址格式不正確
            </div>
          </div>
          <Label for="depth-num" className="m-0">
            欲搜尋層數：
          </Label>
          <div className="d-inline mr-3">
            <Input
              id="depth-num"
              placeholder="層數至少為 1"
              valid={isValidDepth}
              invalid={isInvalidDepth}
              min={1}
              type="number"
              step="1"
              style={{paddingRight: 'calc(1.5em + 0.75rem)'}}
              onChange={updateDepthNum}
              data-testid="depth-num"
            />
            <div className="position-absolute valid-feedback">層數正確</div>
            <div className="position-absolute invalid-feedback">
              請輸入大於等於 1 的整數
            </div>
          </div>
          <Button
            color={btnColor}
            disabled={!allowSubmit || isUploading}
            onClick={createTask}
            data-testid="btn-submit"
          >
            <i className={btnIconClass}></i>
            新增工作
          </Button>
        </div>
      </CardBody>
    </Card>
  );
};

export default WikiCrawlerCreate;
