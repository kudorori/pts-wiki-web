import React from 'react';
import {ListGroupItem, Tooltip, UncontrolledTooltip} from 'reactstrap';
import {getStatusDisplay, taskItem, apiType} from './utils';

type PanelItemEndedProps = {
  task: taskItem;
  type: apiType;
};

const defaultProps = {
  task: {
    index: 0,
    id: 0,
    createdAt: '1900/01/01 00:00:00',
    doneAt: '2020/12/31 23:59:59',
    searchUrl: 'https://zh.wikipedia.org',
    depthNum: 1,
    fileName: 'upload.json',
    faceBoxUrl: 'https://facebox.com/01',
    tagBoxUrl: 'https://tagbox.com/01',
    failedMessage: '',
    progressCount: 0,
    progressTotal: 100,
    status: 'PROCESSING',
  },
  type: 'WikiCrawler',
};

const PanelItemEnded = ({task, type}: PanelItemEndedProps) => {
  const statusDispalyText = getStatusDisplay(task.status).text;
  const statusDispalyPrefix = getStatusDisplay(task.status).textPrefix;
  const statusDispalyColor = getStatusDisplay(task.status).color;

  const infoColForWikiCrawler = (
    task: taskItem
  ): JSX.Element | JSX.Element[] => {
    const searchUrlDisplay = decodeURI(task.searchUrl);
    const exportUrl = `/api/wiki_crawler/list/${task.id}/download`;
    const exportBtn =
      task.status === 'FINISHED' ? (
        <a
          href={exportUrl}
          className="btn btn-info mr-3"
          download
          data-testid="download-link"
        >
          <i className="fas fa-cloud-download-alt mr-2"></i>
          匯出
        </a>
      ) : (
        <a
          className="btn btn-secondary disabled mr-3"
          data-testid="download-link"
        >
          <i className="fas fa-cloud-download-alt mr-2"></i>匯出
        </a>
      );
    return (
      <>
        {exportBtn}
        <a
          href={task.searchUrl}
          target="_blank"
          rel="noreferrer"
          data-testid="search-url"
        >
          {searchUrlDisplay}
        </a>
      </>
    );
  };

  const infoColForTeaching = (task: taskItem): JSX.Element | JSX.Element[] => {
    return (
      <>
        <div className="mr-4 d-flex" style={{minWidth: '160px'}}>
          <span className="d-flex flex-column justify-content-center align-items-end small text-secondary">
            檔案：
          </span>
          <span
            className="d-flex flex-column justify-content-center align-items-start small"
            data-testid="file-name"
          >
            {task.fileName}
          </span>
        </div>
        <div className="d-flex">
          <span
            className="d-flex flex-column justify-content-center align-items-end small"
            style={{minWidth: '100px'}}
          >
            <span className="my-1 text-secondary">FaceBox 網址：</span>
            <span className="my-1 text-secondary">TagBox 網址：</span>
          </span>
          <span className="d-flex flex-column justify-content-center align-items-start small">
            <span className="my-1">{task.faceBoxUrl}</span>
            <span className="my-1">{task.tagBoxUrl}</span>
          </span>
        </div>
      </>
    );
  };

  const infoCol = (type: apiType): Function => {
    switch (type) {
      case 'WikiCrawler':
        return infoColForWikiCrawler;
      case 'Teaching':
        return infoColForTeaching;
    }
  };

  return (
    <ListGroupItem>
      <div className="row fadeInLeft">
        <div
          className="col-1 d-flex justify-content-center align-items-center font-weight-bold"
          data-testid="index"
        >
          {task.index}
        </div>
        <div className="col-7 d-flex justify-content-left align-items-center text-break">
          {infoCol(type)(task)}
        </div>
        <div
          className={`col-1 d-flex flex-column justify-content-center align-items-center small ${statusDispalyColor}`}
        >
          {statusDispalyText}
          {task.status === 'ABORTED' && <span>({task.failedMessage})</span>}
        </div>
        <div className="col-3 d-flex flex-column justify-content-center align-items-center text-secondary small">
          <span className="text-dark">開始於：{task.createdAt}</span>
          <span className={statusDispalyColor}>
            {statusDispalyPrefix}：{task.doneAt}
          </span>
        </div>
      </div>
    </ListGroupItem>
  );
};

PanelItemEnded.defaultProps = defaultProps;

export default PanelItemEnded;
