import React, {useState} from 'react';
import {buildIdHash} from './utils';
import WikiCrawlerCreate from './WikiCrawlerCreate';
import PanelProc from './PanelProc';
import PanelEnded from './PanelEnded';

const WikiCrawler = () => {
  const [idHash, updateIdHash] = useState(buildIdHash([]));
  const [needUpdatePanel, setNeedUpdatePanel] = useState(Date.now());
  return (
    <>
      <WikiCrawlerCreate
        createApi="/api/wiki_crawler"
        setNeedUpdatePanel={setNeedUpdatePanel}
      />
      <PanelProc
        pollingApi="/api/wiki_crawler/process_list"
        pollingIntervalSec={3}
        abortApi="/api/wiki_crawler/list"
        type="WikiCrawler"
        updateIdHash={updateIdHash}
        needUpdatePanel={needUpdatePanel}
      />
      <PanelEnded
        idHash={idHash}
        api="/api/wiki_crawler/list"
        type="WikiCrawler"
      />
    </>
  );
};

export default WikiCrawler;
